#ifndef WORKAROUND
#define __bf_shf(x) (__builtin_ffsll(x) - 1)
#else
#define __bf_shf(x) (__builtin_ctzll(x))
#endif

#define BUILD_BUG_ON_ZERO(e)  ((int)(sizeof(struct { int:(-!!(e)); })))

#define CHECK(_mask)  BUILD_BUG_ON_ZERO(((_mask) >> __bf_shf(_mask)) & 0UL)
#define MYMASK  0x8000000000000000UL

int main(void)
{
	return CHECK(MYMASK);
}
