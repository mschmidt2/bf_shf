.PHONY: test clean

CFLAGS = -g -Wall -O2 -fsanitize=shift

test: bf_shf.gcc bf_shf.i bf_shf.gcc-wa bf_shf.clang
	./bf_shf.gcc
	./bf_shf.gcc-wa
	./bf_shf.clang
	@echo OK

bf_shf.i: bf_shf.c
	gcc $< -E -o $@

bf_shf.gcc: bf_shf.c bf_shf.i
	cat bf_shf.i
	gcc   ${CFLAGS} $< -o $@

bf_shf.gcc-wa: bf_shf.c
	gcc   ${CFLAGS} -DWORKAROUND $< -o $@

bf_shf.clang: bf_shf.c
	clang ${CFLAGS} $< -o $@

clean:
	rm -f ./bf_shf.gcc ./bf_shf.i ./bf_shf.gcc-wa ./bf_shf.clang
