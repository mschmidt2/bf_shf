# bf\_shf

A reproducer for a gcc bug. This came up while working on `include/linux/bitfield.h` in the Linux kernel.

Reproducible with:
gcc (GCC) 13.3.1 20240522 (Red Hat 13.3.1-1)

No longer reproducible with:
gcc (GCC) 14.1.1 20240522 (Red Hat 14.1.1-4)

git bisection of gcc concluded that the fix is:
```
commit 0d00385eaf72ccacff17935b0d214a26773e095f
Author: Jakub Jelinek <jakub@redhat.com>
Date:   Thu Oct 12 16:01:12 2023 +0200

    wide-int: Allow up to 16320 bits wide_int and change widest_int precision to 32640 bits [PR102989]
```

This is what the bug looks like with gcc 13.3.1:

```
$ make
gcc bf_shf.c -E -o bf_shf.i
cat bf_shf.i
# 0 "bf_shf.c"
# 0 "<built-in>"
# 0 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 0 "<command-line>" 2
# 1 "bf_shf.c"
# 15 "bf_shf.c"
int main(void)
{
 return ((int)(sizeof(struct { int:(-!!(((0x8000000000000000UL) >> (__builtin_ffsll(0x8000000000000000UL) - 1)) & 0UL)); })));
}
gcc   -g -Wall -O2 -fsanitize=shift bf_shf.c -o bf_shf.gcc
bf_shf.c: In function ‘main’:
bf_shf.c:7:52: error: bit-field ‘<anonymous>’ width not an integer constant
    7 | #define BUILD_BUG_ON_ZERO(e)  ((int)(sizeof(struct { int:(-!!(e)); })))
      |                                                    ^
bf_shf.c:9:23: note: in expansion of macro ‘BUILD_BUG_ON_ZERO’
    9 | #define CHECK(_mask)  BUILD_BUG_ON_ZERO(((_mask) >> __bf_shf(_mask)) & 0UL)
      |                       ^~~~~~~~~~~~~~~~~
bf_shf.c:17:16: note: in expansion of macro ‘CHECK’
   17 |         return CHECK(MYMASK);
      |                ^~~~~
make: *** [Makefile:16: bf_shf.gcc] Error 1
```
